import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = (props) => (
    <View style={style.constainer}>
        <Text style={style.title}>{ props.title }</Text>
    </View>
);

/* styleSheet */
const style = StyleSheet.create ({
    constainer: {
        marginTop: 25,
        backgroundColor: '#6ca2f7',

        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 50,
        color: '#fff'
    }
});

export default Header;